# Simplon Project #CDA

## Project Overview :

Exercice d'apprentissage React Native



## ScreenShot :

![./assets/ScreenShot_Forecast_ExpoApp.png](./assets/ScreenShot_Forecast_ExpoApp.png)



## How To Run :

Open project folder in a terminal and run following scripts :

```shell
yarn install
```

```
yarn run start
```

!!! Need to install expo on mobile device to see the app !!!



## Consignes

- ## 🤖 App.js

  ```react
import React from 'react';
  import { StyleSheet, Text, View } from 'react-native';
  import { Provider } from 'react-redux'
  import store from './store';
  import Forecast from './components/Forecast';
  
  export default function App() {
    return (
      <Provider store={store}>
          <Forecast />
      </Provider>
    );
}
  ```