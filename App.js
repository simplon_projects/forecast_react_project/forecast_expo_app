import React from 'react';
import { Provider } from 'react-redux';
import store from './src/store'
import Forecast from './src/components/Forecast'

export default function App() {
  return (
    <Provider store={store}>
      <Forecast />
    </Provider>
  );
}