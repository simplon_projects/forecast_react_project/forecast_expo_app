import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { updateCity } from '../actions/forecast';
import { StyleSheet, View, Image, Text, TextInput, Button } from 'react-native';

const styles = StyleSheet.create({
    input: {
        backgroundColor: "white",
    },
    search: {
        resizeMode:"contains",
        width: 10,
        height: 10
    }
});

class ForecastChooseCity extends Component {
    render() {
        return (
            <View>
                <TextInput style={styles.input} value={this.props.city}
                        onChange={(event) => { this.props.updateCity(event.target.value) }}
                    />
                        <Button style={styles.search} title="Search" onPress={this.props.onPress}>
                            <Image source={require('../assets/search.png') } />
                        </Button>
                   
                       
                {this.props.loader &&
                    <Text>Loading...</Text>
                }
                  
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        city: state.forecast.city,
        loader: state.forecast.loader
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateCity
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastChooseCity);