import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
    title: {
        fontSize: 64, 
        fontWeight: 'bold',
        textAlign: 'center',
        color: 'white'
    },
    subtitle: {
        fontSize: 22,
        color: 'white',
        textAlign: 'center'
    }
});

class ForecastTitle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date().toLocaleDateString('fr-fr', { weekday: 'short', day: 'numeric', month: 'short' }),
        }
    }
    render() {
        return (
            <View>
                <Text style={styles.title}>{this.props.city}</Text>
                <Text style={styles.subtitle}>{this.state.date}</Text>
            </View>
        )
    }
}

export default ForecastTitle;