import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { StyleSheet, View, Image, Text } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flexDirection: "column",
        backgroundColor: 'white',
        justifyContent: "center",
        margin: 30,
        borderRadius: 15,
    },
    row: {
        flexDirection: "row",
        minHeight: 100,
    },
    subContainer: {
        minHeight: 100,
        backgroundColor: '#404491',
        flexDirection: "row",
        borderRadius: 5,
        color:'white',
        minHeight: 35,
        padding: 10
    },
    subRow: {
        flexDirection:"row",
        minWidth: 100,
        margin: 20
    },
    description: {
        fontFamily: 'Montserrat',
        fontSize: 18,
        color: '#404491',
        margin: 15
    },
    temperature: {
        fontFamily: 'Montserrat',
        fontSize: 56,
        color: '#404491',
    },
    tinyDesc: {
        fontFamily: 'Montserrat',
        fontSize: 14,
        color: 'white',
    },
    image: {
        resizeMode: "stretch",
        height: 100,
        width: 100
    },
    tinyImg: {
        resizeMode: "stretch",
        width: 30,
        height: 30
    },
});

class ForecastResult extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.row}>
                    {this.props.forecast.current !== undefined &&
                        <Image source={{ uri: this.props.forecast.current.weather_icons[0] }} style={styles.image} />
                    }
                    <View>{this.props.forecast.current !== undefined &&
                        <Text style={styles.description}>{this.props.forecast.current.weather_descriptions}</Text>
                    }</View>
                </View>
                <View style={styles.row}>
                    <View>{this.props.forecast.current !== undefined &&
                        <Text style={styles.temperature}>{this.props.forecast.current.temperature} °C</Text>
                    }</View>
                </View>

                <View style={styles.subContainer}>
                    <View style={styles.subRow}>
                        <Image source={require('../assets/Wind.png')} style={styles.tinyImg} />
                        {this.props.forecast.current !== undefined &&
                            <Text style={styles.tinyDesc}>{this.props.forecast.current.wind_speed} km/h</Text>
                        }</View>

                    <View style={styles.subRow}>
                        <Image source={require('../assets/RainSmall.png')} style={styles.tinyImg} />
                        {this.props.forecast.current !== undefined &&
                            <Text style={styles.tinyDesc}>{this.props.forecast.current.humidity} %</Text>
                        }</View>
                </View>
            </View>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        forecast: state.forecast.forecast
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ForecastResult);
