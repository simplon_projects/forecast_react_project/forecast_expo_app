import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchForecast } from '../actions/forecast';
import ForecastChooseCity from './ForecastChooseCity';
import ForecastResult from './ForecastResult';
import ForecastTitle from './ForecastTitle';
import { StyleSheet, View, ImageBackground } from 'react-native';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: 'white',
        margin: 30,
        borderRadius: 15
    },
    bgImage: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    },
    result: {
        
    },
    choose: {
        flex: 1,
        flexDirection: "row"
    }
});

class Forecast extends Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../assets/background.png')} style={styles.bgImage}>
                    <ForecastTitle city={this.props.city} />
                    <ForecastResult style={styles.result} />
                    <ForecastChooseCity style={styles.choose} onPress={() => { this.props.fetchForecast(this.props.city) }} />
                </ImageBackground>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        forecast: state.forecast.forecast,
        city: state.forecast.city
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchForecast,
    }, dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(Forecast);
