const INITIAL_STATE = {
    forecast: {},
    city: 'Paris',
    loader: false
}

function forecast(state = INITIAL_STATE, action) {
    switch(action.type) {
        case 'UPDATE_FORECAST':
            return {
                ...state,
                forecast: action.value
            }
        case 'UPDATE_CITY':
            return {
                ...state,
                city: action.value
            }
        case 'UPDATE_LOADER':
            return {
                ...state,
                loader: action.status
            }
        default:
            return state;
    }
}

export default forecast;